#!/usr/bin/env python3

"""Generate weekly schedules."""

import datetime
import json
from pathlib import Path

import openpyxl
import pymsgbox

# from IPython.core.debugger import set_trace


# Module Constants
SHEET_NAMES = ("Monday", "Tuesday",
               "Wednesday", "Thursday",
               "Friday")
ROOM_COL = 'A'
TRAY_COL = 'B'
COUNT_COL = 'C'
INFO_COL = 'G'
START_ROW = 1  # index, not actual row
DATE_ROW = 1
PLANTS_TRAY_ROW = 4
TOTAL_IDX = 12
TOTAL_HEADER = "TOTAL"
BLANKS = (None, "", [], (), {})
# Boolean
YES_ANSWERS = ['y', 'Y', '']
NO_ANSWERS = ['n', 'N']
# Files/Path
ROOMS_JSON = str(Path("data/Bloom.json").resolve())
TRAYS_PER_DAY_JSON = str(Path("data/Trays per Day.json").resolve())
TRAYS_PER_ROOM_JSON = str(Path("data/Trays per Room.json").resolve())
EMPTY_MOVE_SCHEDULE = str(Path("data/Move Schedule (Empty).xlsx").resolve())
# # Load information from file # #
with open(ROOMS_JSON) as json_file:
    ROOMS = json.load(json_file)  # list
with open(TRAYS_PER_DAY_JSON) as json_file:
    TRAYS_PER_DAY = json.load(json_file)  # int
with open(TRAYS_PER_ROOM_JSON) as json_file:
    TRAYS_PER_ROOM = json.load(json_file)  # dict


def next_monday():
    """Get the date for the next occurring Monday."""
    # ((Day - Today) + 7) % 7 give the weekday index
    days_after = 7
    today = datetime.date.today()
    days_ahead = days_after - today.weekday()
    return today + datetime.timedelta(days=days_ahead)


def input_last_move() -> dict:
    """Ask which room was last placed, then if the room was fully or
    partially moved. If fully moved, remaining trays is set to
    0. If partially moved, ask how many trays were placed, then use
    that number and the number of trays in the room to calculate
    remaining trays.
    :return a dict containing the last room and remaining trays."""
    # Get last room placed
    input_room_msg = "Enter the last room placed at the end of this week: "
    last_room = pymsgbox.prompt(input_room_msg).upper()
    # While input answer is not valid
    while True:
        full = pymsgbox.prompt("Was it a full room? Y/n: ")
        if full in YES_ANSWERS:
            return {"Room": last_room, "Trays": 0}  # 0 remaining_trays
        elif full in NO_ANSWERS:
            input_trays_msg = "Enter the number of trays already placed: "
            trays_placed = int(pymsgbox.prompt(input_trays_msg))
            # Calculate remaining trays
            return {"Room": last_room,
                    "Trays": int(TRAYS_PER_ROOM[last_room]) - trays_placed}
        else:
            pymsgbox.alert("Option not recognized. Trying again...")
            # continue


def main():
    monday = next_monday()
    date = datetime.date.strftime(monday, "%m.%d.%Y")
    rooms_json = "data/Bloom.json"
    trays_per_room_json = "data/Trays per Room.json"
    empty_schedule = "data/Move Schedule (Empty).xlsx"
    output_schedule = f"{date} Move Cycle.xlsx"
    with open("data/Trays per Day.json") as json_file:
        Trays_per_Day = json.load(json_file)

    # Template Constants
    Sheetnames = ("Monday", "Tuesday",
                  "Wednesday", "Thursday",
                  "Friday")
    Room_Col = 'A'
    Tray_Col = 'B'
    Count_Col = 'C'
    Info_Col = 'G'
    Start_Row = 1  # index, not actual row
    Date_Row = 1
    PlantsperTray_Row = 4
    Total_Index = 12
    Total_Header = "TOTAL"
    Blanks = (None, "")

    # Load rooms
    Rooms = dict()
    with open(rooms_json) as json_file:
        Rooms = json.load(json_file)

    # Load trays per room
    Trays = dict()
    with open(trays_per_room_json) as json_file:
        Trays = json.load(json_file)

    # Get last room placed
    Input_Room_Msg = "Enter the last room placed at the end of this week: "
    last_room = input(Input_Room_Msg).upper()

    # Check if the previous room was a partial
    # and determine if trays still need to be placed
    remaining_trays = int()
    while True:
        full = input("Was it a full room? Y/n: ")
        if (full == 'n') or (full == 'N'):
            Input_Trays_Msg = "Enter the number of trays already placed: "
            trays_placed = int(input(Input_Trays_Msg))
            remaining_trays = int(Trays[last_room]) - trays_placed
            break
        elif (full == 'Y') or (full == 'y'):
            remaining_trays = 0
            break
        else:
            print("Option not recognized. Trying again...")

    # Get plants per tray
    plants_per_tray = int(input("Enter the plants per tray: "))
    totals = list(list())
    schedule_sheets = list(list())
    for sheetname in Sheetnames:
        rooms = list()
        tray_counts = list()
        plant_counts = list()
        total_trays = 0
        total_plants = 0

        # Day/Sheet Setup
        total_remaining_trays = Trays_per_Day

        # Add remaining trays from previous partial
        if (remaining_trays > 0):
            plant_count = remaining_trays * plants_per_tray

            rooms.append(last_room)
            tray_counts.append(remaining_trays)
            plant_counts.append(plant_count)

            total_trays += remaining_trays
            total_plants += plant_count
            total_remaining_trays -= remaining_trays
            remaining_trays = 0

        while True:  # total_remaining_trays > 0:
            # Get index of last room used
            lastroom_index = Rooms.index(last_room)

            # Test at the end of the list
            if lastroom_index == (len(Rooms) - 1):
                room_index = 0  # if so start at the beginning
            else:
                room_index = lastroom_index + 1  # else go to the next index

            # Build rooms, tray counts, and plant counts
            room = Rooms[room_index]
            remaining_trays = int(Trays[room])

            # Determine if remaining trays in room are greater than
            # the number of total remaining trays
            difference = total_remaining_trays - remaining_trays
            if difference >= 0:
                plant_count = remaining_trays * plants_per_tray
                tray_count = remaining_trays
                total_remaining_trays -= remaining_trays

            else:
                plant_count = total_remaining_trays * plants_per_tray
                tray_count = total_remaining_trays
                total_remaining_trays = 0
                remaining_trays = int(Trays[room]) - tray_count

            # Add rooms, tray counts, and plant counts to spreadsheet
            rooms.append(room)
            tray_counts.append(tray_count)
            plant_counts.append(plant_count)

            # Update total trays and plants for the day
            total_trays += tray_count
            total_plants += plant_count

            last_room = room
            room_index += 1

            if total_remaining_trays <= 0:
                break

        # tray_counts.append(total_trays)
        # plant_counts.append(total_plants)
        totals.append([total_trays, total_plants])
        schedule_sheets.append([rooms, tray_counts, plant_counts])

    # Open empty workbook
    # optimize? read_only?
    schedule_wb = openpyxl.load_workbook(empty_schedule)
    # Write generated schedule to Excel file
    for index, sheetname in enumerate(Sheetnames):
        schedule = schedule_wb[sheetname]

        rooms = schedule_sheets[index][0]
        trays = schedule_sheets[index][1]
        plants = schedule_sheets[index][2]

        # Add rooms, trays, and counts
        row = Start_Row
        for room in rooms:
            schedule[Room_Col][row].value = room
            row += 1
        row = Start_Row
        for tray_count in trays:
            schedule[Tray_Col][row].value = tray_count
            row += 1
        row = Start_Row
        for plant_count in plants:
            schedule[Count_Col][row].value = plant_count
            row += 1

        # Add totals
        schedule[Tray_Col][Total_Index].value = totals[index][0]
        schedule[Count_Col][Total_Index].value = totals[index][1]

        # Add date
        date = datetime.date.strftime(monday, "%m/%d/%Y")
        schedule[Info_Col][Date_Row].value = date

        # Add plants per tray
        schedule[Info_Col][PlantsperTray_Row].value = plants_per_tray

        # Delete blank lines

        Next_Row = Start_Row + len(rooms)

        # Count blank lines
        row = Next_Row
        value = schedule[Room_Col][row].value
        blank_count = 0
        while value != Total_Header:
            if value in Blanks:
                blank_count += 1
            row += 1
            value = schedule[Room_Col][row].value

        # Delete blank rows
        if blank_count > 0:
            schedule.delete_rows((Next_Row + 1), blank_count)  # (row, n)
            # if blank_count == 0, then it deletes everything below,
            # including the Totals row

        # Increment date
        monday += datetime.timedelta(days=1)

    schedule_wb.save(output_schedule)
    print(f"Output saved to {output_schedule}")

    return


if __name__ == "__main__":
    main()
